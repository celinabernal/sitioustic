<?php
    include_once('solicitar.php');
?>

<!DOCTYPE html>
<html lang="es">

    <head>
		<meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="La USTIC proporcionará al personal académico del Instituto de Biología de la UNAM, los servicios a las tecnologías de la información necesarias en apoyo a sus actividades académicas" />
        <meta name="author" content="Instituto de Biología, UNAM" />

        <title>Unidad de Sistemas de Tecnologías de Información y Comunicación (USTIC)</title>

        <meta property="og:url" content="http://132.248.75.198/~cuenta04/index.php" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="Unidad de Sistemas de Tecnologías de Información y Comunicación (USTIC)" />
        <meta property="og:description" content="La USTIC proporcionará al personal académico del Instituto de Biología de la UNAM, los servicios a las tecnologías de la información necesarias en apoyo a sus actividades académicas" />
        <meta property="og:image" content="http://132.248.75.198/~cuenta04/assets/img/portada.jpg" />
	
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/img/ib.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
    </head>

    <body id="page-top">
		<!-- Masthead-->
        <header class="masthead bg-info text-white text-center">
            <div class="container d-flex align-items-center flex-column">

                <!-- Masthead Heading-->
                <H1 class="masthead-heading text-uppercase mb-0">Unidad de Sistemas y Tecnologías de Información y Comunicación</H1>
                <!-- Icon Divider-->
                <div class="divider-custom divider-light">
                    <div class="divider-custom-line"></div>
                </div>
                <!-- Masthead Subheading-->
                <p class="mb-3 lead">La USTIC proporcionará al personal académico del Instituto de Biología de la UNAM, los servicios a las tecnologías de la información necesarias en apoyo a sus actividades académicas</p>

                <video poster="assets/img/caratula.jpg" controls >
                    <source src="assets/video/video.mov" type="video/mov">
                    <source src="assets/video/video.mp4" type="video/mp4">
                    Tu navegador no es compatible con videos HTML5
                </video>
            </div>
        </header>
		
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">
            <div class="container">
                <div class="mx-2" ><img src="assets/img/Logo_UNAM.png" alt="logo UNAM" width="50" height="50"/></div>
                <div class="mx-3" ><img src="assets/img/Logo_IB.png" alt="logo IB" width="50" height="60"/></div>
                <a class="navbar-brand" href="#page-top">USTIC</a>
                <button class="navbar-toggler text-uppercase font-weight-bold bg-info text-white rounded" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="fas fa-bars"></i> 
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ms-auto">
                        <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded" href="#portfolio">Servicios</a></li>
                        <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded" href="#about">¿Quiénes somos?</a></li>
                        <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded" href="#contact">Solicitud de Servicio</a></li>
                        <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded" href="doc/reglamento.pdf">Reglamento</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        
        <!-- Portfolio Section-->
        <section class="page-section portfolio" id="portfolio">
            <div class="container">
                <!-- Portfolio Section Heading-->
                <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Servicios</h2>
                <!-- Icon Divider-->
                <div class="divider-custom">
                    <div class="divider-custom-line"></div>
                </div>
                <!-- Portfolio Grid Items-->
                <div class="row justify-content-center">
                    <!-- Portfolio Item 1-->
                    <div class="col-md-6 col-lg-6 mb-5">
                        <div class="portfolio-item mx-auto" data-bs-toggle="modal" data-bs-target="#portfolioModal1">
                            <div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                                <div class="portfolio-item-caption-content text-center text-white"><i class="fas fa-plus fa-3x"></i></div>
                            </div>
                            <img class="img-fluid " src="assets/img/portfolio/soporte.png" alt="soporte técnico" />
                        </div>
                    </div>
                    <!-- Portfolio Item 2-->
                    <div class="col-md-6 col-lg-6 mb-5">
                        <div class="portfolio-item mx-auto" data-bs-toggle="modal" data-bs-target="#portfolioModal2">
                            <div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                                <div class="portfolio-item-caption-content text-center text-white"><i class="fas fa-plus fa-3x"></i></div>
                            </div>
                            <img class="img-fluid" src="assets/img/portfolio/redes.png" alt="redes" />
                        </div>
                    </div>
                    <!-- Portfolio Item 3-->
                    <div class="col-md-6 col-lg-6 mb-5">
                        <div class="portfolio-item mx-auto" data-bs-toggle="modal" data-bs-target="#portfolioModal3">
                            <div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                                <div class="portfolio-item-caption-content text-center text-white"><i class="fas fa-plus fa-3x"></i></div>
                            </div>
                            <img class="img-fluid" src="assets/img/portfolio/desarrollo.png" alt="Desarrollo" />
                        </div>
                    </div>
                    <!-- Portfolio Item 4-->
                    <div class="col-md-6 col-lg-6 mb-5 mb-lg-0">
                        <div class="portfolio-item mx-auto" data-bs-toggle="modal" data-bs-target="#portfolioModal4">
                            <div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                                <div class="portfolio-item-caption-content text-center text-white"><i class="fas fa-plus fa-3x"></i></div>
                            </div>
                            <img class="img-fluid" src="assets/img/portfolio/servidores.png" alt="servidores" />
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- About Section-->
        <section class="page-section bg-info text-white mb-0" id="about">
            <div class="container">
                <!-- About Section Heading-->
                <h2 class="page-section-heading text-center text-uppercase text-white">¿Quiénes Somos?</h2>
                <!-- Icon Divider-->
                <div class="divider-custom divider-light">
                    <div class="divider-custom-line"></div>
                </div>
                <!-- About Section Content-->
                <div class="row">
                    <div class="col-lg-2 me-auto">
                        <img class="img-fluid rounded-circle" src="assets/img/portfolio/David.png" alt="David Vázquez Portilla" />
                        <p class="lead mb-4 mt-4">Mtro. David Vázquez Portilla</p><p class="lead mb-4">Coordinador de la Unidad de Sistemas de Tecnologías de Información y Comunicación</p></div>
                    <div class="col-lg-2 me-auto">
                        <img class="img-fluid rounded-circle" src="assets/img/portfolio/celina.png" alt="Celina Bernal Ramírez" />
                        <p class="lead mb-4 mt-4">Ing. Celina del Carmen Bernal Ramírez</p><p class="lead mb-4">Técnico académico titular A</p></div>
                    <div class="col-lg-2 me-auto">
                        <img class="img-fluid rounded-circle" src="assets/img/portfolio/Jorge.png" alt="Jorge López Ibarra" />
                        <p class="lead mb-4 mt-4">M. en S.I. Jorge Gerardo López Ibarra</p><p class="lead mb-4">Técnico académico titular B</p></div>
                    <div class="col-lg-2 me-auto">
                        <img class="img-fluid rounded-circle" src="assets/img/portfolio/Joel.png" alt="Joel Villavicencio Cisneros" />
                        <p class="lead mb-4 mt-4">Ing. Joel Villavicencio Cisneros</p><p class="lead mb-4">Técnico académico titular B</p></div>
                    <div class="col-lg-2 me-auto">
                        <img class="img-fluid rounded-circle" src="assets/img/portfolio/Alfredo.png" alt="Alfredo Wong López" />
                        <p class="lead mb-4 mt-4" >Biól. José Alfredo Wong León</p><p class="lead mb-4"  >Técnico académico titular B</p></div>
                </div>
              <!--  About Section Button -->
                <div class="text-center mt-4">
                    <a class="btn btn-xl btn-outline-light" href="mailto:ustic@ib.unam.mx">
                        <i class="fas fa-download me-2"></i>
                        Email USTIC
                    </a>
                </div>
            </div>
        </section>

        <!-- Contact Section-->
        <section class="page-section" id="contact">
            <div class="container">
                <!-- Contact Section Heading-->
                <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Solicitud de Servicio</h2>
                <!-- Icon Divider-->
                <div class="divider-custom">
                    <div class="divider-custom-line"></div>
                </div>
                <!-- Contact Section Form-->
                <div class="row justify-content-center">
                    <div class="col-lg-8 col-xl-7">

                    <form  method="post">  <!--id="contactForm"-->
    
                        <!-- Name input-->
                            <div class="form-floating mb-3">
                                <input class="form-control" id="inputNombre" type="text" name="nombre" placeholder="Escriba su nombre..." data-sb-validations="required" />
                                <label for="inputNombre">Nombre Completo</label>
                                <div class="invalid-feedback" data-sb-feedback="nombre:required">Un nombre es requerido</div>
                            </div>

                        <!-- Email address input-->
                            <div class="form-floating mb-3">
                                <input class="form-control" id="inputEmail" type="email" name="email" placeholder="name@example.com" data-sb-validations="required,email" />
                                <label for="inputEmail">Correo Electrónico</label>
                                <div class="invalid-feedback" data-sb-feedback="email:required">Un correo electrónico es requerido</div>
                                <div class="invalid-feedback" data-sb-feedback="email:email">Correo electrónico no es válido</div>
                            </div>

                        <!-- Phone number input-->
                            <div class="form-floating mb-3">
                                <input class="form-control" id="inputTelefono" type="tel" name="telefono" placeholder="(123) 456-7890" data-sb-validations="required" />
                                <label for="inputTelefono">Teléfono</label>
                                <div class="invalid-feedback" data-sb-feedback="telefono:required">Un número telefónico es requerido</div>
                            </div>

                        <!-- Ubicacion input-->
                            <div class="form-floating mb-3">
                                <input class="form-control" id="inputUbicacion" type="text" name="ubicacion" placeholder="Escriba su ubicación-cubículo" data-sb-validations="required" />
                                <label for="inputUbicacion">Ubicacion-cubículo</label>
                                <div class="invalid-feedback" data-sb-feedback="ubicacion:required">La ubicación-cubículo es requerido</div>
                            </div>

                        <!-- Message input-->
                            <div class="form-floating mb-3">
                                <textarea class="form-control" id="inputDescripcion" type="text" name="descripcion" placeholder="Escriba la descripción aquí..." style="height: 10rem" data-sb-validations="required"></textarea>
                                <label for="inputDescripcion">Descripción del problema</label>
                                <div class="invalid-feedback" data-sb-feedback="descripcion:required">Una descripción es requerida.</div>
                            </div>

                        <!-- Submit Button-->
                            <input class="btn btn-info btn-xl" id="inputSubmit" type="submit" name="submit" value="Enviar">
                        </form>
                        <h3 class="text-secondary text-center"><b><?php echo $mensajeSuccess; ?></b></h3>
                    </div>
                </div>
            </div>
        </section>
        <!-- Footer-->
        <footer class="footer text-center">
            <div class="container">
                <div class="row">
                    <!-- Footer Location-->
                    <div class="col-lg-4 mb-5 mb-lg-0">
                        <h4 class="text-uppercase mb-4">Ubicación</h4>
                        <p class="lead mb-0">
                            Instituto de Biología, UNAM
                            <br />
                            Edificio B, planta baja
                        </p>
                    </div>
                    <!-- Footer Social Icons-->
                    <div class="col-lg-4 mb-5 mb-lg-0">
                        <h4 class="text-uppercase mb-4"></h4>
                        <a class="btn btn-outline-light btn-social mx-1" href="https://www.facebook.com/Instituto-de-Biolog%C3%ADa-UNAM-175521352466916"><i class="fab fa-fw fa-facebook-f"></i></a>
                        <a class="btn btn-outline-light btn-social mx-1" href="https://twitter.com/ib_unam"><i class="fab fa-fw fa-twitter"></i></a>
                    </div>
                    
                    <!-- Footer About Text-->
                    <div class="col-lg-4">
                        <h4 class="text-uppercase mb-4">Teléfonos</h4>
                        <p class="lead mb-0">
                            5556229106, <br>
                            5556229168
                        </p>
                    </div>

                    <!-- Your share button code -->
                    <div class="fb-share-button" 
                        data-href="http://132.248.75.198/~cuenta04/index.php" 
                        data-layout="button_count">
                    </div>
                    
                </div>
            </div>
        </footer>
        <!-- Copyright Section-->
        <div class="copyright py-4 text-center text-white">
            <div class="container"><small>Copyright &copy; USTIC 2022 Hecho en México, Instituto de Biología de la Universidad Nacional Autónoma de México (UNAM), todos los derechos reservados.</small></div>
        </div>

        <!-- Portfolio Modals-->
        <!-- Portfolio Modal 1-->
        <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" aria-labelledby="portfolioModal1" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header border-0"><button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button></div>
                    <div class="modal-body text-center pb-5">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-8">
                                    <!-- Portfolio Modal - Title-->
                                    <h2 class="portfolio-modal-title text-secondary text-uppercase mb-0">Soporte Técnico</h2>
                                    <!-- Icon Divider-->
                                    <div class="divider-custom">
                                        <div class="divider-custom-line"></div>
                                    </div>
                                    <!-- Portfolio Modal - Image-->
                                    <img class="img-fluid2 rounded mb-5" src="assets/img/portfolio/soporte.png" alt="soporte técnico" />
                                    <!-- Portfolio Modal - Text-->
                                    <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia neque assumenda ipsam nihil, molestias magnam, recusandae quos quis inventore quisquam velit asperiores, vitae? Reprehenderit soluta, eos quod consequuntur itaque. Nam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia neque assumenda ipsam nihil, molestias magnam, recusandae quos quis inventore quisquam velit asperiores, vitae? Reprehenderit soluta, eos quod consequuntur itaque. Nam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia neque assumenda ipsam nihil, molestias magnam, recusandae quos quis inventore quisquam velit asperiores, vitae? Reprehenderit soluta, eos quod consequuntur itaque. Nam.</p>
                                    <button class="btn btn-info" data-bs-dismiss="modal">
                                        <i class="fas fa-xmark fa-fw"></i>
                                        Salir
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Portfolio Modal 2-->
        <div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" aria-labelledby="portfolioModal2" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header border-0"><button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button></div>
                    <div class="modal-body text-center pb-5">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-8">
                                    <!-- Portfolio Modal - Title-->
                                    <h2 class="portfolio-modal-title text-secondary text-uppercase mb-0">Redes</h2>
                                    <!-- Icon Divider-->
                                    <div class="divider-custom">
                                        <div class="divider-custom-line"></div>
                                    </div>
                                    <!-- Portfolio Modal - Image-->
                                    <img class="img-fluid2 rounded mb-5" src="assets/img/portfolio/redes.png" alt="redes" />
                                    <!-- Portfolio Modal - Text-->
                                    <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia neque assumenda ipsam nihil, molestias magnam, recusandae quos quis inventore quisquam velit asperiores, vitae? Reprehenderit soluta, eos quod consequuntur itaque. Nam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia neque assumenda ipsam nihil, molestias magnam, recusandae quos quis inventore quisquam velit asperiores, vitae? Reprehenderit soluta, eos quod consequuntur itaque. Nam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia neque assumenda ipsam nihil, molestias magnam, recusandae quos quis inventore quisquam velit asperiores, vitae? Reprehenderit soluta, eos quod consequuntur itaque. Nam.</p>
                                    <button class="btn btn-info" data-bs-dismiss="modal">
                                        <i class="fas fa-xmark fa-fw"></i>
                                        Salir
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Portfolio Modal 3-->
        <div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" aria-labelledby="portfolioModal3" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header border-0"><button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button></div>
                    <div class="modal-body text-center pb-5">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-8">
                                    <!-- Portfolio Modal - Title-->
                                    <h2 class="portfolio-modal-title text-secondary text-uppercase mb-0">Desarrollo de sistemas y bases de datos distribuidas</h2>
                                    <!-- Icon Divider-->
                                    <div class="divider-custom">
                                        <div class="divider-custom-line"></div>
                                    </div>
                                    <!-- Portfolio Modal - Image-->
                                    <img class="img-fluid2 rounded mb-5" src="assets/img/portfolio/desarrollo.png" alt="desarrollo" />
                                    <!-- Portfolio Modal - Text-->
                                    <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia neque assumenda ipsam nihil, molestias magnam, recusandae quos quis inventore quisquam velit asperiores, vitae? Reprehenderit soluta, eos quod consequuntur itaque. Nam.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia neque assumenda ipsam nihil, molestias magnam, recusandae quos quis inventore quisquam velit asperiores, vitae? Reprehenderit soluta, eos quod consequuntur itaque. Nam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia neque assumenda ipsam nihil, molestias magnam, recusandae quos quis inventore quisquam velit asperiores, vitae? Reprehenderit soluta, eos quod consequuntur itaque. Nam. </p>
                                    <button class="btn btn-info" data-bs-dismiss="modal">
                                        <i class="fas fa-xmark fa-fw"></i>
                                        Salir
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Portfolio Modal 4-->
        <div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" aria-labelledby="portfolioModal4" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header border-0"><button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button></div>
                    <div class="modal-body text-center pb-5">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-8">
                                    <!-- Portfolio Modal - Title-->
                                    <h2 class="portfolio-modal-title text-secondary text-uppercase mb-0">Servidores</h2>
                                    <!-- Icon Divider-->
                                    <div class="divider-custom">
                                        <div class="divider-custom-line"></div>
                                    </div>
                                    <!-- Portfolio Modal - Image-->
                                    <img class="img-fluid2 rounded mb-5" src="assets/img/portfolio/servidores.png" alt="servidores" />
                                    <!-- Portfolio Modal - Text-->
                                    <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia neque assumenda ipsam nihil, molestias magnam, recusandae quos quis inventore quisquam velit asperiores, vitae? Reprehenderit soluta, eos quod consequuntur itaque. Nam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia neque assumenda ipsam nihil, molestias magnam, recusandae quos quis inventore quisquam velit asperiores, vitae? Reprehenderit soluta, eos quod consequuntur itaque. Nam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia neque assumenda ipsam nihil, molestias magnam, recusandae quos quis inventore quisquam velit asperiores, vitae? Reprehenderit soluta, eos quod consequuntur itaque. Nam.</p>
                                    <button class="btn btn-info" data-bs-dismiss="modal">
                                        <i class="fas fa-xmark fa-fw"></i>
                                        Salir
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
<!--        <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script> -->


        <!-- Load Facebook SDK for JavaScript -->
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s); js.id = id;
                    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>


    </body>
</html>
