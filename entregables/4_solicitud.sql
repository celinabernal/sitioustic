CREATE TABLE solicitud (
  id smallint(6) NOT NULL AUTO_INCREMENT,
  nombre varchar(50) NOT NULL,
  email varchar(70) NOT NULL,
  telefono varchar(15) NOT NULL,
  ubicacion varchar(100) NOT NULL,
  descripcion varchar(500) NOT NULL,
  atendido tinyint(1) DEFAULT NULL,
  fecha_reg varchar(15) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB;







